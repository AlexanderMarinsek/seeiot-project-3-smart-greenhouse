# Smart Greenhouse project
(Third project at "Sistemi Embedded e Internet-Of-Things" at Ingegneria e scienze informatiche, Cesena, University of Bologna.)

Arduino IDE (or avr-gcc...), Android Studio, Python 2.7 and Django 1.11 required


## To run (in 'src' folder):

I.) In Mysql create database 'greenhouse' with user 'greenhouse_user' (password '1234qwer') and import 'greenhouse.sql' database

II.) Go to 'sgh-server-client' > 'mysite' and run Django server

III.) In another terminal run the serial interface 'sgh-server-client' > 'st.py'

IV.) Open 'sgh-edge' > 'esp_loop' > 'esp_loop.ino' using Arduino IDE, connect ESP, change destination (server) values and upload. Now the server should start receiving HTTP get requests with ADC values from the ESP.

V.) Open 'sgh-controller' > 'greenhouse' > 'greenhouse.ino' and connect everything as described in 'Pins.h'. Afterwards upload to Arduino Uno board and the app Arduino should start communicating over serial and over Bluetooth.

VI.) For Bluetooth communication with smartphone open and compile 'sgh-mobile-app' > 'SmartGreenHouse_v2', or download 'Arduino Bluetooth Controller' from Google Play store and manually send messages.

VII.) To view latest data in web browser, go to '127.0.0.1/greenhouse/'
