#ifndef __STATEIMP__
#define __STATEIMP__

#include "State.h"
#include "TimerImp.h"

class StateImp: public State {
public:
    StateImp(){};
    virtual ~StateImp(){};
    
    void update_dist_val(int16_t val);

    virtual int16_t check_state() = 0;

    void update_rh (float val);
    float get_rh_value ();
    
    void update_delta_t (unsigned long delta_t);
    unsigned long get_delta_t ();
    
    void update_flux (int16_t flux);
    int16_t flux_neg_edge();
    int16_t flux_no_change();
    
    //void update_drained_water (int16_t drained_water);
    int16_t start_irrigation ();
    int16_t start_bluetooth ();
    int16_t stop_bluetooth ();

    void update_led(int16_t values [3]);
    void get_led(int16_t *led);
    
    void update_bt_connected (int16_t conn);
    void update_bt_disconnect (int16_t conn);
    int16_t is_bt_connected ();
    
protected:
    //bool pir_val = 0;
    //int16_t pot_val = 0;
    int16_t dist_val = 0;
    //int16_t btn_val = 0;
    
    //float rh_value = 0.0;
    float rh_value = 50.0;
    //float rh_value = 20.0;
    int16_t flux = 0;
    int16_t flux_last = 0;
    //int16_t drained_water = 0;
    unsigned long delta_t = 0;
    int16_t led [3] = {0,0,0}; 

    int16_t bt_connected = 0;
    int16_t bt_disconnect = 0;
    
    Timer *timer;
};

#endif
