#include "Task_pump_auto.h"
#include "PumpImp.h"
#include "MsgService.h"
#include "GeneralConst.h"

Task_pump_auto::Task_pump_auto (StateImp **state_pp){
    //locked = false;
    this->state_pp = state_pp;
    this->pump = new PumpImp(PUMP_PIN);
    //this->pump = new PumpFake();
}

Task_pump_auto::~Task_pump_auto (){
    delete this->pump;
}

void Task_pump_auto::run() {
    int16_t flux_tmp;
    float rh = (*this->state_pp)->get_rh_value();
    
    if (!(this->pump->has_timer_finished()) && rh < (RH_MIN + RH_DELTA)){
        if (rh > 20) {
            flux_tmp = FLUX_MIN;
        }
        else if (rh > 10 && rh <= 20) {
            flux_tmp = FLUX_MED;
        }
        else if(rh <= 10) {
            flux_tmp = FLUX_MAX;
        }
    } 
    else {
        flux_tmp = 0;
    } 
    
    int16_t led [3] = {255,flux_tmp*80,0};
    (*this->state_pp)->update_led(led);    
    
    Serial.print("flux_tmp: ");
    Serial.println(flux_tmp);
    // Open pump accordingly
    this->pump->open_pump(flux_tmp);

    // Update current flux, drained water amount and delta t
    (*this->state_pp)->update_delta_t(this->pump->get_delta_t());
    (*this->state_pp)->update_flux(flux_tmp);
    //(*this->state_pp)->update_drained_water(this->pump->get_drained_water());
}
