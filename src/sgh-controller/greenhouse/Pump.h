#ifndef __PUMP__
#define __PUMP__


#include <stdint.h>


class Pump { 
public: 
    virtual void open_pump(int16_t flux) = 0;
    virtual void close_pump() = 0;
    virtual int16_t has_timer_finished() = 0;
    virtual unsigned long get_delta_t() = 0;
    //virtual int16_t get_drained_water() = 0;
};


#endif
