#include "Arduino.h"
#include "MsgService.h"


String content;


MsgServiceClass MsgService;


// Triggered event hadnler on incoming message
void serialEvent() {
    unsigned long st_t = micros();
    // Loop input buffer
    while (Serial.available()) {
        char ch = (char) Serial.read();
        if (ch == '\n'){
            //MsgService.s_content = content;
            //MsgService.msgAvailable = true;    
            MsgService.rh_value = MsgService.get_num_in_msg(content, CHECK_STR);
            //Serial.print("Elapsed: ");
            //Serial.println(micros() - st_t);
            // Echo back received content
            #if ECHO == 1
            Serial.print("echo: ");
            Serial.println(content);
            #endif
            content = "";
        } else {
            content += ch;      
        }
    }
}


void MsgServiceClass::init(){
    Serial.begin(9600);
    content.reserve(MSG_LEN);
    content = "";
    //this->s_content.reserve(MSG_LEN);
    //this->s_content = "";
    //this->msgAvailable = false;  
}


void MsgServiceClass::send_rh_request(){
    //this->s_content = "";
    char str [MSG_LEN] = {0};
    str_formatter(str, 0, 1);
    Serial.println(str);
    Serial.flush();
    return;
}


void MsgServiceClass::send_delta_t(int delta_t){
    char str [MSG_LEN] = {0};
    str_formatter(str, delta_t, 0);
    Serial.println(str);
    Serial.flush();
    return;
}


void MsgServiceClass::str_formatter(char *str, int delta_t, int get_rh){
    //sprintf(str, "aaa: %d", 13);
    sprintf(str, "{\"%s\":%d, \"%s\":%d}", 
      "dt", delta_t, 
      "rh", get_rh
    );
    return;
}


float MsgServiceClass::get_rh(){
    return this->rh_value;
}


// Get number (RH value) in message
float MsgServiceClass::get_num_in_msg (String str, char *check_str) {
    int i = 0;
    int k = 0;

    // Iterate message string
    for (i=0; i<MSG_LEN; i++) {
        // Found matching character
        if (str[i] == check_str[k]) {
            k++;
            // Reached end of check substring
            if (k == CHECK_MSG_LEN) {
                //Serial.print("Substring found, k: ");
                //Serial.println(k);
                // Go to next character (after check_str)
                //k++;
                // Stop for loop
                break;
            }
        } 
        // Mismatch, reset substring counter
        else {
            k = 0;
        }
    }

    // Substring not found
    if (i == MSG_LEN) {
        return 0.0;
    }

    // At this point 'i' points to last char of 'check_str' within 'str'
  
    char buf_str [BUF_MSG_LEN+1] = { 0 };
    int digits = 0;
    int decimals = 0;
    int dec_flag = 0;

    int n = 0;
    
    for (n; n<(MSG_LEN-i); n++) {
        if (isdigit(str[n+1+i])) {
            buf_str[n] = str[n+1+i];
            // Start counting decimals
            if (dec_flag) {
                decimals++;
            }
            digits++;
        } 
        // Decimal poitn reached
        else if (str[n+1+i] == DECIMAL_POINT_SYMBOL) {
            buf_str[n] = str[n+1+i];
            dec_flag = 1;
        } 
        else {
            break;
        }
    }

    float sum = 0.0;
    int tmp;
    int dide = digits - decimals;

    for (i=n-1; i>=0; i--) {
        tmp = buf_str[i] - '0';
        if (i > dide) {
            sum += tmp * pow(10, dide-i);
        }
        else if (i == dide) {
            // Decimal point
        }
        else if (i < (digits - decimals)) {
            sum += tmp * pow(10, dide-i-1);
        }
    }

    return sum;
}


/*
bool MsgServiceClass::isMsgAvailable(){
    return msgAvailable;
}

String MsgServiceClass::receiveMsg(){
    if (this->msgAvailable){
        content = "";
        this->msgAvailable = false;
        Serial.print("this->s_content: ");
        Serial.println(this->s_content);
        return this->s_content;  
    } else {
        return NULL; 
    }
}


void MsgServiceClass::sendMsg(const String& msg){
    Serial.println(msg);  
}


void MsgServiceClass::receive_rh(){
    this->receiveMsg();
    this->rh_value = get_num_in_msg(this->s_content, CHECK_STR);
}
*/
