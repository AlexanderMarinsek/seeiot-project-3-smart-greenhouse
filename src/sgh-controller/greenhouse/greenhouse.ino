
#include "StateDef.h"
#include "Task_state_m.h"
#include "Scheduler.h"
#include "Tasks.h"
#include "GeneralConst.h"
#include "Pins.h"
#include "MsgService.h"

StateImp *state;

Scheduler *sched = new Scheduler();

void setup() {
    MsgService.init();
    Serial.begin(9600); 

    //pinMode(LED_PIN_1, OUTPUT);
    //pinMode(LED_PIN_2, OUTPUT);
    //pinMode(LED_PIN_3, OUTPUT);
    state = new Idle();

    sched->add_task(new Task_state_m(&state, sched));
    sched->add_task(new Task_led(&state));
    sched->add_task(new Task_get_rh(&state));
    sched->add_task(new Task_read_dist(TRIG_PIN, ECHO_PIN, &state));
    
    /*
    state = new Irrigate_manual();

    sched->add_task(new Task_state_m(&state, sched));
    sched->add_task(new Task_led(&state));
    sched->add_task(new Task_bluetooth(&state));
    */

    
    Serial.println("** Init complete **");
    delay(3000);
}


void loop() {

    sched->run();

}
