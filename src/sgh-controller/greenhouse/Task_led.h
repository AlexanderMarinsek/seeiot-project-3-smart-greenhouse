#ifndef __TASK_LED__
#define __TASK_LED__

#include <stdint.h>
#include "Task.h"
#include "Pins.h"
#include "Led.h"
#include "LedExt.h"


class Task_led: public Task {
public:
    Task_led(StateImp **state_pp);
    ~Task_led ();
    void run();
private:
    Led *led_1;
    LedExt *led_2;
    Led *led_3;
};


#endif
