#include "Task_bluetooth.h"


Task_bluetooth::Task_bluetooth (StateImp **state_pp){
    this->msgServiceBt = new MsgServiceBT(BT_RX_PIN, BT_TX_PIN);
    this->msgServiceBt->init();  
    this->pump = new PumpImp(PUMP_PIN);
    //this->pump = new PumpFake();
    this->state_pp = state_pp;
}

Task_bluetooth::~Task_bluetooth () {
    delete this->pump;
    delete this->msgServiceBt;
}

void Task_bluetooth::run() {
    // Update drained water
    //this->bluetooth->update_drained_water(this->pump->get_drained_water());
    this->msgServiceBt->update_rh((*this->state_pp)->get_rh_value());

    this->msgServiceBt->send_rh();

    int16_t flux = this->msgServiceBt->get_flux();

    //Serial.print("# pump->get_drained_water(): ");
    //Serial.println(this->pump->get_drained_water());
    Serial.print("# msgServiceBt->get_flux(): ");
    Serial.println(this->msgServiceBt->get_flux());
    Serial.print("# msgServiceBt->is_bt_connected(): ");
    Serial.println(this->msgServiceBt->is_bt_connected());
    Serial.print("# msgServiceBt->do_bt_disconnect(): ");
    Serial.println(this->msgServiceBt->do_bt_disconnect());
    
    int16_t led [3] = {0,flux*80,255};
    (*this->state_pp)->update_led(led);
    
    Serial.print("flux: ");
    Serial.println(flux);
    // Open pump accordingly
    this->pump->open_pump(flux);
    
    (*this->state_pp)->update_bt_connected(this->msgServiceBt->is_bt_connected());
    (*this->state_pp)->update_bt_disconnect(this->msgServiceBt->do_bt_disconnect());
}
