#ifndef __MSGSERVICE__
#define __MSGSERVICE__

#include <ctype.h>
#include <math.h>
#include "Arduino.h"


#define SERIAL_IDLE             0
#define SERIAL_SEND_DELTA       1
#define SERIAL_GET_RH           2
#define SERIAL_AWAITING_DELTA   3
#define SERIAL_AWAITING_RH      


#define CHECK_STR               "\"rh\":"

#define MSG_LEN                 256
#define CHECK_MSG_LEN           5
#define BUF_MSG_LEN             16
#define DECIMAL_POINT_SYMBOL    '.'

#define ECHO                    1


class MsgServiceClass {

public: 

  void init();  
  void send_rh_request();
  void send_delta_t(int delta_t);
  
  void str_formatter (char *str, int delta_t, int get_rh);
  float get_num_in_msg (String str, char *check_str);
  
  float get_rh();
  
  //bool isMsgAvailable();
  //String receiveMsg();
  //void sendMsg(const String& msg);
  //void receive_rh();

  // Don't start with rh=0 -> avoid irrigation when turning on
  //float rh_value = 0.0;
  float rh_value = 50.0;
  //float rh_value = 20.0;
  
  //bool msgAvailable = false;
  //String s_content;

  
};

extern MsgServiceClass MsgService;

#endif
