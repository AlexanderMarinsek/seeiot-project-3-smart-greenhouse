#include "Task_get_rh.h"
#include "MsgService.h"


Task_get_rh::Task_get_rh (StateImp **state_pp){
    this->state_pp = state_pp;
    //locked = false;
    //this->pin = pin;
    //this->pot = new PotImp(POT_PIN);
    //this->pot = new PotFake();
}

Task_get_rh::~Task_get_rh (){
    //delete this->pot;
}

void Task_get_rh::run() {
    //(*this->state_pp)->update_pot_val(this->pot->get_value());
    //MsgService.sendPot(this->pot->get_value());
    
    MsgService.send_rh_request();
    // Get latest RH (updates automatically using event handler)
    (*this->state_pp)->update_rh(MsgService.get_rh());
}
