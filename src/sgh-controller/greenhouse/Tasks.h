#ifndef __TASKS__
#define __TASKS__

#include "Task_state_m.h"
#include "Task_get_rh.h"
#include "Task_read_dist.h"
#include "Task_pump_auto.h"
#include "Task_led.h"
#include "Task_bluetooth.h"

#endif
