#ifndef __TIMERIMP__
#define __TIMERIMP__


#include "Timer.h"
#include <stdint.h>


class TimerImp: public Timer {
private:
    unsigned long period_ms;
    unsigned long start_time;
    unsigned long last_read;
public:
    TimerImp(unsigned long period_ms);
    int16_t has_finished();
    unsigned long elapsed();
    unsigned long since_last_read();
};


#endif
