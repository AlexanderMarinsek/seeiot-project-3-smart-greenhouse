#ifndef __TASK_BLUETOOTH__
#define __TASK_BLUETOOTH__

#include "Task.h"
#include "StateImp.h"
#include "MsgServiceBT.h"
#include "PumpImp.h"
#include "Pins.h"
#include <stdint.h>


class Task_bluetooth: public Task {
private:
    //Bluetooth *bluetooth;
    MsgServiceBT *msgServiceBt;
    Pump *pump;
public:
    Task_bluetooth(StateImp **state_pp);
    ~Task_bluetooth ();
    void run();
};


#endif
