#ifndef __SONAR__
#define __SONAR__


#include <stdint.h>


class Sonar {
public:
    virtual int16_t get_distance();
};


#endif
