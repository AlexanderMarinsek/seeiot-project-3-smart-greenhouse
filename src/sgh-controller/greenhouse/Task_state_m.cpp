#include "Task_state_m.h"
#include "Arduino.h"

#include "StateDef.h"
#include "Tasks.h"

#include "MsgService.h"

#include "Pins.h"


int16_t num_of_coffee = INITIAL_NUM_OF_COFFEE;


//Task_state_m::Task_state_m (State* state){
Task_state_m::Task_state_m (StateImp **state_pp, Scheduler *sched_p){
    this->locked = true;
    this->state_pp = state_pp;
    this->sched_p = sched_p;
}

void Task_state_m::run() {
    int16_t state_change = (*this->state_pp)->check_state();

    Serial.print("State current: ");
    Serial.println((*this->state_pp)->current_state_mask);
    
    Serial.print("State change: ");
    Serial.println(state_change);

    if (state_change == IDLE_MASK) {
        Serial.println("IDLE_MASK");
        
        delete *this->state_pp; 
        *this->state_pp = new Idle();
        
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_led(this->state_pp));
        this->sched_p->add_task(new Task_get_rh(this->state_pp));
        this->sched_p->add_task(new Task_read_dist(TRIG_PIN, ECHO_PIN, this->state_pp));
    } 
    else if (state_change == IDLE_BT_MASK) {
        delete *this->state_pp; 
        *this->state_pp = new Idle_bt();
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_led(this->state_pp));
        this->sched_p->add_task(new Task_get_rh(this->state_pp));
        this->sched_p->add_task(new Task_read_dist(TRIG_PIN, ECHO_PIN, this->state_pp));
        this->sched_p->add_task(new Task_bluetooth(this->state_pp));
        
        Serial.println("IDLE_BT_MASK");
    }
    else if (state_change == IRRIGATE_AUTO_MASK) {
        delete *this->state_pp; 
        *this->state_pp = new Irrigate_auto();
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_led(this->state_pp));
        this->sched_p->add_task(new Task_get_rh(this->state_pp));
        this->sched_p->add_task(new Task_pump_auto(this->state_pp));
        
        Serial.println("IRRIGATE_AUTO_MASK");
    }
    else if (state_change == IRRIGATE_MANUAL_MASK) {
        delete *this->state_pp; 
        *this->state_pp = new Irrigate_manual();
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_led(this->state_pp));
        this->sched_p->add_task(new Task_bluetooth(this->state_pp));
        
        Serial.println("IRRIGATE_MANUAL_MASK");
    }
    else if (state_change == NO_CHANGE_MASK) {
        // nothing
    }
    
}
