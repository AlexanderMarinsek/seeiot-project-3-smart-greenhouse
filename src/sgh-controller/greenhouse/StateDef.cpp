#include "StateDef.h"
#include "TimerImp.h"
#include "MsgService.h"
#include "Pins.h"
#include "Arduino.h"
//#include <avr/sleep.h>


#include "GeneralConst.h"




Idle::Idle() {
  this->current_state_mask = IDLE_MASK;
    int16_t led [3] = {255,0,0};
    memcpy(this->led, led, sizeof(led));
}

Idle::~Idle() {
}

int16_t Idle::check_state() {
    
    if (this->start_irrigation()) {
        return IRRIGATE_AUTO_MASK;
    } 
    else if (this->start_bluetooth()) {
        return IDLE_BT_MASK;
    } 
    else {
        return NO_CHANGE_MASK;
    }
}


Idle_bt::Idle_bt() {
    this->current_state_mask = IDLE_BT_MASK;
    int16_t led [3] = {0,0,255};
    memcpy(this->led, led, sizeof(led));
}

Idle_bt::~Idle_bt() {
}

int16_t Idle_bt::check_state() {

    if (this->start_irrigation()) {
        return IRRIGATE_AUTO_MASK;
    } 
    else if (this->stop_bluetooth()) {
        return IDLE_MASK;
    } 
    else if (this->is_bt_connected()) {
        return IRRIGATE_MANUAL_MASK;
    }
    else {
        return NO_CHANGE_MASK;
    }
}


Irrigate_auto::Irrigate_auto() {
    this->current_state_mask = IRRIGATE_AUTO_MASK;
    int16_t led [3] = {255,0,0};
    memcpy(this->led, led, sizeof(led));
}

Irrigate_auto::~Irrigate_auto() {
}

int16_t Irrigate_auto::check_state() {

    if (this->flux_neg_edge()) {
        MsgService.send_delta_t(this->get_delta_t());
        return IDLE_MASK;
    } else {
        return NO_CHANGE_MASK;
    }
}


Irrigate_manual::Irrigate_manual() {
    this->current_state_mask = IRRIGATE_MANUAL_MASK;
    int16_t led [3] = {0,0,255};
    memcpy(this->led, led, sizeof(led));
}

Irrigate_manual::~Irrigate_manual() {
}

int16_t Irrigate_manual::check_state() {

    if (this->bt_disconnect) {
        return IDLE_BT_MASK;
    } else {
        return NO_CHANGE_MASK;
    }
}
