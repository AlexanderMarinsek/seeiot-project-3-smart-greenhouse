#ifndef __TASK_READ_AUTO__
#define __TASK_READ_AUTO__

#include <stdint.h>
#include "Task.h"
#include "PumpImp.h"
#include "Pins.h"
#include "GeneralConst.h"


class Task_pump_auto: public Task {
private:
    Pump *pump;
public:
    Task_pump_auto(StateImp **state_pp);
    ~Task_pump_auto ();
    void run();
};


#endif
