#include "Task_led.h"


Task_led::Task_led (StateImp **state_pp){
    this->state_pp = state_pp;
    this->led_1 = new Led(LED_PIN_1);
    this->led_2 = new LedExt(LED_PIN_2, 0);
    this->led_3 = new Led(LED_PIN_3);
}

Task_led::~Task_led (){
    this->led_1->switchOff();
    this->led_2->switchOff();
    this->led_3->switchOff();
    delete this->led_1;
    delete this->led_2;
    delete this->led_3;
}

void Task_led::run() {
    int16_t led [3];
    (*this->state_pp)->get_led(led);

    if (led[0] == 0) {
      this->led_1->switchOff();
    } else {
      this->led_1->switchOn();
    }
    
    if (led[1] == 0) {
      this->led_2->switchOff();
    } else {
      this->led_2->setIntensity((int)led[1]);
      this->led_2->switchOn();
    }
    
    if (led[2] == 0) {
      this->led_3->switchOff();
    } else {
      this->led_3->switchOn();
    }
    
    //analogWrite(LED_PIN_1, led[0]);
    //analogWrite(LED_PIN_2, led[1]);
    //analogWrite(LED_PIN_3, led[2]);
    
}
