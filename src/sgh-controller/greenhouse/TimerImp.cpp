#include "TimerImp.h"
#include "Arduino.h"
#include <stdint.h>


TimerImp::TimerImp(unsigned long period_ms) {
    this->period_ms = period_ms;
    this->start_time = millis();
    this->last_read = this->start_time;
}


int16_t TimerImp::has_finished() {
    if (millis() - this->start_time >= this->period_ms) {
        return 1;
    } else {
        return 0;
    }
}


unsigned long TimerImp::elapsed() {
    return (millis() - this->start_time);
}


unsigned long TimerImp::since_last_read() {
    return (millis() - this->last_read);
}
