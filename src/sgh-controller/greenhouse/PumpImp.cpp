#include "PumpImp.h"
#include "Arduino.h"


static volatile uint8_t on_time, off_time;
static volatile uint8_t ISRCount;

static volatile uint8_t servo_pin;


ISR (TIMER2_OVF_vect)
{ 
  ISRCount++;
  
  if (ISRCount == on_time) {
      digitalWrite(servo_pin, LOW);
  } 
  else if (ISRCount == off_time) {
      ISRCount = 0;
      digitalWrite(servo_pin, HIGH);
  }
  
  /*
  if (ISRCount <= on_time) {
      digitalWrite(SERVO_PIN, HIGH);
  } 
  else if (ISRCount > off_time) {
      ISRCount = 0;
      //TIMSK2 = 0;
  }
  else if (ISRCount > on_time) {
      digitalWrite(SERVO_PIN, LOW);
  }
  */
}


PumpImp::PumpImp (int outputPin){
  servo_pin = outputPin;
  this->pin = outputPin;
  pinMode(this->pin, OUTPUT);
  digitalWrite(this->pin, LOW);
  this->timer = new TimerImp(PUMP_MAX_DELTA_T);
} 


PumpImp::~PumpImp(){
  delete this->timer;    
  digitalWrite(this->pin, LOW);
  Serial.println("Delete pump");
} 


void PumpImp::open_pump(int16_t flux) {  
  // Regulate servo
  switch (flux) {
    case 0:
      on_time = CLOSE_ON_TIME;
      break;
    case 1:
      on_time = MIN_ON_TIME;
      break;
    case 2:
      on_time = MED_ON_TIME;
      break;
    case 3:
      on_time = MAX_ON_TIME;
      break;
    default: 
      on_time = CLOSE_ON_TIME;
      break;
  }

  off_time = ON_OFF_SUM - on_time;

  initISR();
    
  return;
}


void PumpImp::close_pump() {
  this->open_pump(0);
  return 0;
}


int16_t PumpImp::has_timer_finished() {
  return this->timer->has_finished();
}


unsigned long PumpImp::get_delta_t() {
  return this->timer->elapsed();
}

/*
int16_t PumpImp::get_drained_water() {
  return this->drained_water;
}
*/


static void initISR()
{     
  ISRCount = 0;  // clear the value of the ISR counter;
  // Setup for timer 2 
  TIMSK2 = 0;  // disable interrupts 
  TCCR2A = 0;  // normal counting mode 
  //TCCR2B = _BV(CS21); // set prescaler of 8 
  // *** Desired frequency 2 kHz**
  //TCCR2B &= 0xF8;     // clear CS (prescaler - bottom 3) bits
  TCCR2B = 0x00;   // Prescaler = 0
  //TCCR2B |= 0x04;   // Prescaler = 64
  TCCR2B |= 0x02;   // Prescaler = 8
  //OCR2B = 0x7c;     // OCR = 124
  //OCR2B = 0x3d;       // 61
  OCR2B = 0xFF;     // OCR = 255
  TCNT2 = 0;     // clear the timer2 count 
  TIFR2 = _BV(TOV2);  // clear pending interrupts; 
  TIMSK2 =  _BV(TOIE2) ; // enable the overflow interrupt   
} 


static void stopISR() {
  ISRCount = 0;  // clear the value of the ISR counter;
  TIMSK2 = 0;  // disable interrupts 
  TCNT2 = 0;     // clear the timer2 count 
  TIFR2 = _BV(TOV2);  // clear pending interrupts; 
}


///////////////////////////////////////////////////////////

PumpFake::PumpFake(){
  this->timer = new TimerImp(PUMP_MAX_DELTA_T);
} 


PumpFake::~PumpFake(){
  delete this->timer;     
} 


void PumpFake::open_pump(int16_t flux) {
  /*this->flux = flux;
   Get drained amount since last read
  this->drained_water = 
      (int16_t)(this->flux_last * this->timer->since_last_read() * 0.001);
  return;*/
}


void PumpFake::close_pump() {
  this->flux = 0;
  return;
}


unsigned long PumpFake::get_delta_t() {
  return this->timer->elapsed();
}


int16_t PumpFake::has_timer_finished() {
  return this->timer->has_finished();
}

/*
int16_t PumpFake::get_drained_water() {
  return this->drained_water;
}
*/
