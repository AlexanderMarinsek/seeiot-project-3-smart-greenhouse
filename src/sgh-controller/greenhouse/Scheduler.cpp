#include "Scheduler.h"
#include "Arduino.h"


void Scheduler::add_task (Task *task) {
    if (num_of_tasks < MAX_TASK_NUM) {
        task_arr[num_of_tasks] = task;
        num_of_tasks++;
    }
} 

/*
 *  Release space for all non-locked tasks 
 */
void Scheduler::clear_tasks () {
    //Serial.println("Clear tasks");
    int16_t i=0;
    int16_t k=0;
    for (i=0; i<num_of_tasks; i++) {
        /*Serial.print("is locked: ");
        Serial.println(task_arr[i]->is_locked());*/
        if (task_arr[i]->is_locked()){
            /*Serial.print("is locked: ");
            Serial.println(task_arr[i]->is_locked());*/
            task_arr[k] = task_arr[i];
            k++;
        } else {
            delete task_arr[i];
        }
    }
    /*Serial.print("k: ");
    Serial.println(k);*/
    num_of_tasks = k;
    task_counter = 0;
}


void Scheduler::run() {
    /*Serial.print("num_of_tasks: ");
    Serial.println(num_of_tasks);
    Serial.print("task_counter: ");
    Serial.println(task_counter);
    Serial.print("address: ");
    Serial.println((int)task_arr[task_counter]);*/
    if (num_of_tasks > 0) {
        task_arr[task_counter]->run();
        //task_counter = (task_counter + 1) % num_of_tasks;
        if (task_counter==0) {
          this->last_start = millis();
        }
        task_counter++;
        if (task_counter>=num_of_tasks) {
            task_counter=0;
            this->wait();
        }
    }
}


/*
 * Wait for the rest of period (normally would go to sleep)
 */
void Scheduler::wait() {
    while (millis() - last_start < SCHED_PERIOD_MS) {
        // loop
    }
    
}
