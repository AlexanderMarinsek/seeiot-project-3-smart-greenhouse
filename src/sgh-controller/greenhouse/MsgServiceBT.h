#ifndef __MSGSERVICEBT__
#define __MSGSERVICEBT__

#include "Arduino.h"
#include "SoftwareSerial.h"


#define CHECK_STR               "\"flux\":"

#define MSG_LEN                 256
#define CHECK_MSG_LEN           7
#define BUF_MSG_LEN             16

#define MAX_BT_RETRIES          10

/*
class Msg {
  String content;

public:
  Msg(const String& content){
    this->content = content;
  }
  
  String getContent(){
    return content;
  }
};
*/

class MsgServiceBT {
    
public: 
  MsgServiceBT(int rxPin, int txPin);  
  ~MsgServiceBT ();
  void init();  
  bool isMsgAvailable();
  //bool sendMsg(String str);
  bool sendMsg(char *str);
  void receiveMsg();
  String getReceived();
  
  void send_rh();
  void update_rh(float rh_val);
  int16_t get_flux();
  
  int16_t is_bt_connected();
  int16_t do_bt_disconnect();

private:
  int16_t get_num_in_msg (String str, char *check_str);
  
  String content;
  String received;
  float rh = 0;
  int16_t flux = 0;
  
  int16_t bt_connected = 0;
  int16_t bt_disconnect = 0;
  int16_t bt_counter = 0;
  
  SoftwareSerial* channel;
  
};

#endif
