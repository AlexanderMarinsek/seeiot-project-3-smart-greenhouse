
#include "Arduino.h"
#include "StateImp.h"
#include "GeneralConst.h"



/*
void StateImp::update_drained_water(int16_t val) {
  this->drained_water = val;
  Serial.print("drained_water: ");
  Serial.println(val);
}
*/
void StateImp::update_delta_t(unsigned long val) {
  this->delta_t = val;
  Serial.print("delta_t: ");
  Serial.println(val);
}

unsigned long StateImp::get_delta_t() {
    return this->delta_t;
}

void StateImp::update_flux(int16_t val) {
  this->flux_last = this->flux;
  this->flux = val;
  Serial.print("flux: ");
  Serial.println(val);
}

int16_t StateImp::flux_neg_edge() {
  
    Serial.print("this->flux: ");
    Serial.println(this->flux);
    Serial.print("this->flux_last: ");
    Serial.println(this->flux_last);
    
    if (this->flux == 0 && this->flux_last > 0) {
        return 1;
    }
    return 0;
}

int16_t StateImp::flux_no_change() {
  if (this->flux == 0 && this->flux_last == 0) {
      return 1;
  }
  return 0;
}


void StateImp::update_dist_val(int16_t val) {
  this->dist_val = val;
  Serial.print("Update dist, val: ");
  Serial.println(val);
  return;
}


void StateImp::update_rh(float val) {
    this->rh_value = val;
    Serial.print("Update rh, val: ");
    Serial.println(val);
    return;
}


int16_t StateImp::start_irrigation() {
    //Serial.print("this->rh_value: ");
    //Serial.println(this->rh_value);
    if (this->rh_value < RH_MIN) {
        return 1;
    } else {
        return 0;
    }
}


void StateImp::update_led(int16_t values [3]) {
    this->led[0] = values [0];
    this->led[1] = values [1];
    this->led[2] = values [2];
}


void StateImp::get_led(int16_t *led) {
    memcpy(led, this->led, sizeof(this->led));
    return;
}


int16_t StateImp::start_bluetooth() {
    return (this->dist_val > 0);
}


int16_t StateImp::stop_bluetooth() {
    return (this->dist_val == 0);
}


float StateImp::get_rh_value() {
    return this->rh_value;
}


void StateImp::update_bt_connected(int16_t conn) {
    this->bt_connected = conn;
    Serial.print("this->bt_connected: ");
    Serial.println(this->bt_connected);
    return;
}


void StateImp::update_bt_disconnect(int16_t conn) {
    this->bt_disconnect = conn;
    return;
}


int16_t StateImp::is_bt_connected() {
    return this->bt_connected;
}
