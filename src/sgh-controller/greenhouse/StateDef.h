#ifndef __STATEDEF__
#define __STATEDEF__

#include "StateImp.h"


#define NO_CHANGE_MASK            -1
#define IDLE_MASK                 0
#define IDLE_BT_MASK              1
#define IRRIGATE_AUTO_MASK        2
#define IRRIGATE_MANUAL_MASK      3


class Idle: public StateImp {
public:
    Idle();
    ~Idle();
    int16_t check_state();
};


class Idle_bt: public StateImp {
public:
    Idle_bt();
    ~Idle_bt();
    int16_t check_state();
};


class Irrigate_auto: public StateImp {
public:
    Irrigate_auto();
    ~Irrigate_auto();
    int16_t check_state();
};


class Irrigate_manual: public StateImp {
public:
    Irrigate_manual();
    ~Irrigate_manual();
    int16_t check_state();
};


#endif
