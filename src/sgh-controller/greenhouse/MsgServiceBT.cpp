#include "Arduino.h"
#include "MsgServiceBT.h"


MsgServiceBT::MsgServiceBT(int rxPin, int txPin){
  this->channel = new SoftwareSerial(rxPin, txPin);
}


MsgServiceBT::~MsgServiceBT(){
  delete this->channel;
}


void MsgServiceBT::init(){
  //content.reserve(MSG_LEN);
  //received.reserve(MSG_LEN);
  channel->begin(9600);
}


//bool MsgServiceBT::sendMsg(String str){
bool MsgServiceBT::sendMsg(char *str){
  Serial.print("BT sent message: "); 
  Serial.println(str); 
  channel->println(str);  
}


bool MsgServiceBT::isMsgAvailable(){
  return channel->available();
}


void MsgServiceBT::receiveMsg(){
  Serial.println("receiveMsg2");
  while (channel->available()) {
    char ch = (char) channel->read();
    // Delay at least 50us, otherwise messages are gibberish
    delayMicroseconds(500);
    //Serial.print("ch: ");
    //Serial.println(ch);
    //if (ch == '\n'){
    if (ch == '#'){
      this->received = content;
      this->flux = this->get_num_in_msg(received, CHECK_STR);
      Serial.print("received: ");
      Serial.println(this->received);
      Serial.print("this->flux: ");
      Serial.println(this->flux);
      //this->sendMsg("echo: " + this->received);
      content = "";
      return;
    } else {
      content += ch;      
    }
  }
  content = "";
  return;  
}


String MsgServiceBT::getReceived(){
  return this->received;  
}


void MsgServiceBT::send_rh(){
  char buff [32] = {0};
  sprintf(buff, "{\"rh\":%03d}#", (int)(this->rh*10));
  this->sendMsg(buff);
  
  if (this->isMsgAvailable()) {
    this->receiveMsg();  
    this->bt_counter = 0;
    this->bt_connected = 1;
    this->bt_disconnect = 0;
  } 
  else {
    this->bt_counter++;
  }

  if (this->bt_counter > MAX_BT_RETRIES) {
    this->bt_connected = 0;
    this->bt_disconnect = 1;
  }
  
  return;
}


int16_t MsgServiceBT::is_bt_connected(){
  return this->bt_connected;
}


int16_t MsgServiceBT::do_bt_disconnect(){
  return this->bt_disconnect;
}


void MsgServiceBT::update_rh(float rh_val){
  this->rh = rh_val;
  return;
}


int16_t MsgServiceBT::get_flux(){
  return this->flux;
}


// Get number (flux value) in message
int16_t MsgServiceBT::get_num_in_msg (String str, char *check_str) {
    int16_t i = 0;
    int16_t k = 0;

    // Iterate message string
    for (i=0; i<MSG_LEN; i++) {
        // Found matching character
        if (str[i] == check_str[k]) {
            k++;
            // Reached end of check substring
            if (k == CHECK_MSG_LEN) {
                //Serial.print("Substring found, k: ");
                //Serial.println(k);
                // Go to next character (after check_str)
                //k++;
                // Stop for loop
                break;
            }
        } 
        // Mismatch, reset substring counter
        else {
            k = 0;
        }
    }

    // Substring not found
    if (i == MSG_LEN) {
        return 0;
    }

    // At this point 'i' points to last char of 'check_str' within 'str'
  
    char buf_str [BUF_MSG_LEN+1] = { 0 };
    int16_t digits = 0;

    int16_t n = 0;
    
    for (n; n<(MSG_LEN-i); n++) {
        if (isdigit(str[n+1+i])) {
            buf_str[n] = str[n+1+i];
            digits++;
        } 
        else {
            break;
        }
    }

    int16_t sum = 0;
    int16_t tmp;

    for (i=n-1; i>=0; i--) {
        tmp = (int16_t) (buf_str[i] - '0');
        // Problems with integers greater than 99 (returns one less)
        sum += (int16_t) (tmp * pow(10, digits-i-1));
    }

    return sum;
}
