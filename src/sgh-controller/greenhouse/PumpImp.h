#ifndef __PUMPIMP__
#define __PUMPIMP__

#include <stdint.h>
#include "Arduino.h"

#include <stdint.h>
#include "Pump.h"
#include "TimerImp.h"
//#include"ServoTimer2.h"
//#include <Servo2.h>


#define PUMP_MAX_DELTA_T    5000
#define FLUX_MIN            1     // m^3/s
#define FLUX_MED            2
#define FLUX_MAX            3

#define CLOSE_ON_TIME       5
#define MIN_ON_TIME         9
#define MED_ON_TIME         14
#define MAX_ON_TIME         18

/*
// P = 64, OCR = 124
#define ON_OFF_SUM  40    // 20 ms
#define ON_TIME     2
#define OFF_TIME    ON_OFF_SUM - ON_TIME 
*/
/*
// P = 64, OCR = 61
#define ON_OFF_SUM  80    // 20 ms
#define ON_TIME     1
#define OFF_TIME    ON_OFF_SUM - ON_TIME 
*/

// P = 8, OCR = 255
#define ON_OFF_SUM  156    // 20 ms
//#define ON_TIME     18
//#define OFF_TIME    ON_OFF_SUM - ON_TIME 


static void initISR();
static void stopISR();


class PumpImp: public Pump {
  
  protected:
    int16_t pin = 0;
    //int16_t pump_state = 0;
    int16_t flux = 0;
    //int16_t flux_last = 0;
    //int16_t drained_water = 0;
    int16_t delta_t = 0;
    Timer *timer;
    
  public: 
    PumpImp(int outputPin);
    ~PumpImp();
    void open_pump(int16_t flux);
    void close_pump();
    unsigned long get_delta_t();
    int16_t has_timer_finished();
    //int16_t get_drained_water();
    //ServoTimer2 *servo;
    //Servo2 *servo;
};


class PumpFake: public Pump {
  
  protected:
    int16_t pin = 0;
    //int16_t pump_state = 0;
    int16_t flux = 0;
    //int16_t flux_last = 0;
    //int16_t drained_water = 0;
    int16_t delta_t = 0;
    Timer *timer;
    
  public: 
    PumpFake();
    ~PumpFake();
    void open_pump(int16_t flux);
    void close_pump();
    unsigned long get_delta_t();
    int16_t has_timer_finished();
    //int16_t get_drained_water();
};


#endif
