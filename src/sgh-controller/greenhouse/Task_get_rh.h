#ifndef __TASK_GET_RH__
#define __TASK_GET_RH__

#include "Task.h"
#include "StateImp.h"
#include "Pins.h"
#include <stdint.h>


class Task_get_rh: public Task {
private:
    //int16_t pin;
    //Pot *pot;
public:
    Task_get_rh(StateImp **state_pp);
    ~Task_get_rh ();
    void run();
};


#endif
