#ifndef __SCHEDULER__
#define __SCHEDULER__

#include "Runnable.h"
#include "Task.h"
#include <stdint.h>

#define MAX_TASK_NUM        10
//#define SCHED_PERIOD_MS     100
#define SCHED_PERIOD_MS     1000

class Scheduler {
protected:
    int16_t num_of_tasks = 0;
    int16_t task_counter = 0;
    unsigned long last_start = 0;
    Task *task_arr[MAX_TASK_NUM];
    void wait();
public:
    void add_task (Task *task);
    void clear_tasks ();
    void run();
};

#endif
