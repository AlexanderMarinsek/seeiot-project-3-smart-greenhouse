#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

#define NEW_REQUEST_TIME_MS   1000

#define REQUEST_LEN           1024
#define RESPONSE_LEN          1024
#define REQUEST_STR_SIZE      256

#define SOCKET_CLOSED         0
#define SOCKET_CONNECTED      1
#define SOCKET_SENT           2
#define SOCKET_RECEIVED       2
  
const char* ssid = "dev";
const char* password = "filadelfija";

const char* host = "10.0.0.5";
const int httpsPort = 80;
             
char  request[REQUEST_LEN+1],
      response[RESPONSE_LEN+1],
      request_data[REQUEST_STR_SIZE+1];
  
char request_fmt[REQUEST_LEN] = 
      "GET /greenhouse/upload/%s HTTP/1.1\r\n"
      "Host: %s\r\n"
      "Content-Type: application/x-www-form-urlencoded\r\n"
      "Content-Length: 0\r\n\r\n"
      "\r\n";

int elapsed = 0;
int last_sent = 0;
float rh = 0;

int socket_state = 0;

WiFiClient client;


void setup() {
  pinMode (A0, INPUT);
  
  Serial.begin(115200);
  Serial.println();
  Serial.print("connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void loop() {

  if (millis() - last_sent > NEW_REQUEST_TIME_MS) {
    elapsed = 1;
  }

  if (elapsed == 1) {
    
    if (socket_state == SOCKET_CLOSED) {
      Serial.println("Connecting");
      client.connect(host, httpsPort);
      socket_state = SOCKET_CONNECTED;
    }
    
    else if (socket_state == SOCKET_CONNECTED) {
      Serial.println("Sending");
      rh = analogRead(A0) / 1023.0 * 100.0;
      if (rh > 99.9) {rh=99.9;}
      //sprintf(request_data, "?rh=%.1f", rh);
      sprintf(request_data, "?dt=0&rh=%.1f", rh);
      int request_data_len = strlen(request_data);
      sprintf(request, request_fmt, request_data, host);
          
      Serial.println("Request: ");
      Serial.println(request);
      client.print(request);
      //socket_state = SOCKET_SENT;
      
      // Even if we don't really close it
      socket_state = SOCKET_CLOSED;
      last_sent = millis();
      elapsed = 0;
    }
    /*
    else if (socket_state == SOCKET_SENT) {
      Serial.println("Receiving");
      String line = client.readStringUntil('\n');
      if (line == "\r") {
        Serial.println("headers received");
        Serial.println(line);
      }
    }
    
    else if (socket_state == SOCKET_RECEIVED) {
      Serial.println("'Closing'");
      // Even if we don't really close it
      socket_state = SOCKET_CLOSED;
      last_sent = millis();
    }
    */
    else {
      //
    }
    
  }
  
  delay(100);
}
