# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import timedelta

from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from models import Humidity, Irrigation


def index(request):
    print request
    
    #query_results = Humidity.objects.all()
    humidity_query_results = Humidity.objects.order_by('-id')[:10]
    irrigation_query_results = Irrigation.objects.order_by('-id')[:10]
    
    template = loader.get_template('greenhouse/index.html')
    context = {
        'humidity_query_results': humidity_query_results,
        'irrigation_query_results': irrigation_query_results
    }
    
    #return HttpResponse("Hello, world. You're at the polls index.")
    return HttpResponse(template.render(context, request))


def upload(request):

    rh_tmp = dt_tmp = None;
    
    if request.method == 'GET':
        rh_tmp = float(request.GET.get('rh', None))
        dt_tmp = int(request.GET.get('dt', None))
    
    elif request.method == 'POST':
        rh_tmp = float(request.POST.get('rh', None))
        dt_tmp = int(request.POST.get('dt', None))
   
    else:
        return HttpResponse('0')
    
    # Save params to DB and return ID
    return HttpResponse(save_params(rh_tmp, dt_tmp))


def latest_rh(request):
    
    if request.method == 'GET':
        #rh = 90
        rh = Humidity.objects.latest('id')
        return HttpResponse(rh)
    else:
        return HttpResponse('0')


def save_params(rh_tmp, dt_tmp):
    if rh_tmp:
        print "Save RH: ", rh_tmp
        rh = float(rh_tmp)
        h = Humidity(rh_value = rh)
        h.save()
        return h.id
    elif dt_tmp:
        print "Save DT: ", dt_tmp
        dt = float(dt_tmp)
        d = timedelta(milliseconds = int(dt))
        i = Irrigation(delta_t = d)
        i.save()
        return i.id
    else:
        return 0
