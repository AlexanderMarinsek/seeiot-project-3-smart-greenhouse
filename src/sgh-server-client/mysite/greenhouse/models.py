# -*- coding: utf-8 -*-
from __future__ import unicode_literals


# Create your models here.V
import datetime

from django.db import models
from django.utils import timezone

class Humidity (models.Model):
    
    # Timestamp from when object was created
    timestamp = models.DateTimeField(default=timezone.now)
    # Format xx.x
    rh_value = models.DecimalField(max_digits=3, decimal_places=1)

    def __str__(self):
        #return str(self.timestamp)
        return str(self.rh_value)

class Irrigation (models.Model):
    
    # Timestamp from when object was created
    timestamp = models.DateTimeField(default=timezone.now)
    # Format 'timedelta'
    delta_t = models.DurationField()

    def __str__(self):
        #return str(self.timestamp)
        return str(self.delta_t)

