from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    #url(r'greenhouse/', views.index, name='index'),
    url(r'upload/', views.upload, name='upload'),
    url(r'latest_rh/', views.latest_rh, name='latest_rh'),
]
