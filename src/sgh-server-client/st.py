import serial
import time
import json
import requests
from datetime import timedelta

#ser = serial.Serial('/dev/ttyACM0', 9600)
ser = serial.Serial('/dev/ttyACM1', 9600)
print ser.name

while True:
    line = ser.readline()
    if line:
        # Try JSON decoding received message
        try:
            rec_obj = json.loads(line)
            print "JSON: ", rec_obj
            if rec_obj['rh'] == 1:
                print "Get RH"
                # Try getting RH from server
                try:
                    r = requests.get( \
                        'http://10.0.0.5:80/greenhouse/latest_rh/')
                    print 'Request.get: ', r.text
                    rh = float(r.text)
                except:
                    print "Can't connect to server"
                    rh = 0.0
                response = '{\"%s\":%d, \"%s\":%.1f}' % (
                    'dt', 0, 
                    'rh', rh)
                ser.write("%s\n" % response)
                print "Read: ", rec_obj
                print "Write: ", response
                #ser.flush()
            elif rec_obj['rh'] == 0:
                ms = rec_obj['dt']
                print "Update DT: ", ms
                #d = timedelta(microseconds=ms)
                try:
                    r = requests.get(
                        'http://10.0.0.5:80/greenhouse/upload/', 
                        params = { 
                            'dt': ms,
                            'rh': 0.0 
                        } 
                    )
                    print "Request: ", r.text
                except:
                    print "Couldn't issue new request"
        except:
            print "Not JSON format."
        print "Line: ", line
    time.sleep(0.01)
    
