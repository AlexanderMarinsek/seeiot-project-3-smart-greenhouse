package seiot.smartgreenhouse_v2;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothHeadset;
import android.graphics.ColorSpace;
import android.os.Bundle;

import java.util.UUID;

public class ServerMainActivity extends Activity {
    private static final String APP_UUID = "c4daba7b-14c1-4cc1-b117-87ff24b9f21d";
    private BluetoothAdapter btAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        //init bt
    }

    public void onStart() {
        super.onStart();
        UUID uuid = UUID.fromString(APP_UUID);
        //MasterThread th = new MasterThread(uuid, "MyBTApp");
        //th.start();
    }

    public void onDestroy(String msg){
        super.onDestroy();
        ConnectionManager.getInstance().cancel();
    }

    public void sendMessage(String msg){
        ConnectionManager.getInstance().write(msg.getBytes());
    }

}
