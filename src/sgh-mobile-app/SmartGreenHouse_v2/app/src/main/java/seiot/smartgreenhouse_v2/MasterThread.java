package seiot.smartgreenhouse_v2;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.UUID;

public class MasterThread extends Thread{
    private boolean connectionAccepted = false;
    private boolean stop = false;
    private BluetoothSocket socket;
    private ConnectionManager cm;
    private volatile EditText umidita;
    private volatile MainActivity ma;

    public MasterThread(UUID uuid, BluetoothDevice btDevice, MainActivity mainActivity){
        BluetoothAdapter btAdapter;
        ma = mainActivity;
        umidita = ma.getEditText();
        this.umidita.setText("50.0");
        Log.e("MyApp", "in thread: " + umidita.toString());
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        try{
            //Log.e("MyApp", btDevice.getName().toString() + " " + uuid.toString());
            //serverSocket = btAdapter.listenUsingRfcommWithServiceRecord(btDevice.getName(), uuid);
            socket = btDevice.createInsecureRfcommSocketToServiceRecord(uuid);
            //Log.e("MyApp", String.valueOf(serverSocket==null));
            //Log.e("MyApp", serverSocket.toString());
            //Log.e("MyApp", "socket creata");
            //Log.e("MyApp", serverSocket.);
        } catch(IOException e){
            //Log.e("MyApp", "eccezione costruttore");
            e.printStackTrace();
        }
    }

    public void run(){
        while(!connectionAccepted && !stop){

            //Log.e("MyApp", "dentro while");
            try{
                //Log.e("MyApp", "try");
                socket.connect();
                //Log.e("MyApp", "after");
            } catch(IOException e){
                //Log.e("MyApp", "eccezione accettazione");
                e.printStackTrace();
                stop = true;
            }
            //Log.e("MyApp", "0");
            if(socket != null) {
                //Log.e("MyApp", "socket ok");
                connectionAccepted = true;
                //Log.e("MyApp", "1");
                cm = ConnectionManager.getInstance();
                //Log.e("MyApp", "2");
                cm.setChannel(socket, ma);
                //Log.e("MyApp", "3");
                cm.start();
                //Log.e("MyApp", " start");
                //byte msg = Byte.valueOf("a");
                //byte[] message = {0x8, 0x7};


                //byte[] message = {0x48, 0x65, 0x6c, 0x6c, 0x6f};


                //message[0]

                //Log.e("MyApp", "send m");


                //cm.write(message);
            }else{
                //Log.e("MyApp", "error");
            }

            /*
            try{
                //serverSocket.close();
                //socket.close();
            } catch(IOException e){
                stop = true;
            }
            */
        }
        //Log.e("MyApp", "fuori while");
    }

    public void setFlux(int flux){
        cm.setFlux(flux);
    }
}
