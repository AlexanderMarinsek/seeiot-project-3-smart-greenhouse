package seiot.smartgreenhouse_v2;

import android.bluetooth.BluetoothSocket;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ConnectionManager extends Thread {
    private BluetoothSocket btSocket;
    private InputStream btInputStream;
    private OutputStream btOutputStream;
    private boolean stop;
    private static ConnectionManager instance = null;
    private int flux = 0;
    private ConnectionManager(){
        stop = true;
    }
    private volatile EditText umidità;
    private double umid = 0;


    public static ConnectionManager getInstance(){
        if(instance == null){
            instance = new ConnectionManager();
        }
        return instance;
    }

    public void setChannel(BluetoothSocket socket){
        //this.umidità = mainActivity.getEditText();

        btSocket = socket;
        try{
            btInputStream = socket.getInputStream();
            btOutputStream = socket.getOutputStream();

        } catch(IOException e){

        }
        stop = true;
    }

    public void run(){
        byte[] buffer = new byte[1024];
        String msg = "";
        int nBytes = 0;
        Log.e("MyApp", "run");
        stop = false;
        while(!stop){
        //while(true){
            try{
                Log.e("MyApp", "------------- new arrival ------------");

                nBytes = btInputStream.read(buffer);
                for(byte b : buffer){
                    if(b!=0 && b != 35){
                        //Log.e("MyApp", Character.toString((char) b));
                        //Log.e("MyApp", String.valueOf(b));
                        msg += Character.toString((char) b);
                    } else if (b == 35){
                        //msg like {"rh":xxx}#  just consider xxx
                        int i = msg.indexOf(":");
                        int num = Integer.valueOf(msg.charAt(i+1))*10;
                        num += Integer.valueOf(msg.charAt(i+2));
                        num += Integer.valueOf(msg.charAt(i+3))/10;
                        this.umid = num;

                        //umidità.setText(String.valueOf(num));

                        //umidità.setText("20.1"); check this


                        Log.e("MyApp", msg);
                        msg="";
                        switch(flux){
                            //7b 22 66 6c 75 78 22 3a 31 7d 23 = flux = 1
                            case 0:
                                byte[] message = {0x7b, 0x22, 0x66, 0x6c, 0x75, 0x78, 0x22, 0x3a, 0x30, 0x7d, 0x23};
                                write(message);
                                break;

                            case 1:
                                byte[] message1 = {0x7b, 0x22, 0x66, 0x6c, 0x75, 0x78, 0x22, 0x3a, 0x31, 0x7d, 0x23};
                                write(message1);
                                break;

                            case 2:
                                byte[] message2 = {0x7b, 0x22, 0x66, 0x6c, 0x75, 0x78, 0x22, 0x3a, 0x32, 0x7d, 0x23};
                                write(message2);
                                break;

                            case 3:
                                byte[] message3 = {0x7b, 0x22, 0x66, 0x6c, 0x75, 0x78, 0x22, 0x3a, 0x33, 0x7d, 0x23};
                                write(message3);
                                break;

                            default:
                                byte[] message4 = {0x7b, 0x22, 0x66, 0x6c, 0x75, 0x78, 0x22, 0x3, 0x30, 0x7d, 0x23};
                                write(message4);
                                break;
                        }



                    }
                }
                Arrays.fill(buffer, (byte)0);
            }catch(IOException e){

                Log.e("MyApp", e.toString());
                try {
                    btSocket.connect();
                }catch (IOException e1){
                    Log.e("MyApp", e1.toString());
                }
                //stop = true;
            }
        }
        //Log.e("MyApp", "end");
    }

    public boolean write(byte[] bytes){
        if (btOutputStream == null) return false;
        try{
            btOutputStream.write(bytes);
            btOutputStream.flush();
        } catch (IOException e){
            return false;
        }
        return true;
    }

    public String getHumidity(){

        //ONLY FOR DEBUG
        //Random rand = new Random();
        //double n = rand.nextDouble(); // Gives n such that 0 <= n < 20

        return String.valueOf(this.umid);
        //return String.valueOf(n);


    }


    public void setFlux(int flux1){
        this.flux = flux1;
    }

    public void cancel(){
        try{
            btSocket.close();
        } catch(IOException e){

        }
    }
}
