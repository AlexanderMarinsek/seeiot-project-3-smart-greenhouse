package seiot.smartgreenhouse_v2;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    private BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
    private static final String myUUID = "00001101-0000-1000-8000-00805F9B34FB";
    private String BT_TARGET_NAME = "isi00";
    private BluetoothDevice device;
    private UUID uuid;
    private MasterThread th;

    private int currFlux = 0;
    private Button button0;
    private Button button1;
    private Button button2;
    private Button button3;

    private TextView portata;
    private volatile EditText umidita;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        currFlux = 0;
        button0 = findViewById(R.id.button0);
        button0.setEnabled(true);
        button1 = findViewById(R.id.button1);
        button1.setEnabled(true);
        button2 = findViewById(R.id.button2);
        button2.setEnabled(true);
        button3 = findViewById(R.id.button3);
        button3.setEnabled(true);
        umidita = findViewById(R.id.txtUmidita);
        umidita.setEnabled(false);
        Log.e("MyApp", "in ma: " + umidita.toString());

        /*
        E/MyApp: in ma: android.support.v7.widget.AppCompatEditText{3f5c5b50 VF.D..CL ......I. 0,0-0,0 #7f070092 app:id/txtUmidita}
        E/MyApp: in thread: android.support.v7.widget.AppCompatEditText{3f5c5b50 VF.D..CL ......I. 0,0-0,0 #7f070092 app:id/txtUmidita}
        E/MyApp: in cm: android.support.v7.widget.AppCompatEditText{3f5c5b50 VF.D..CL .F...... 0,29-510,97 #7f070092 app:id/txtUmidita}
         */

        portata = findViewById(R.id.portata);
        portata.setText("Portata: " + currFlux);

        //check if BT is supported
        if(btAdapter==null){
            //Log.e("MyApp", "BT is not available on this device");
            finish();
        }
        //check if BT is enabled
        if(!btAdapter.isEnabled()){
            /*Intent i = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(i, ENABLE_BT_REQ);*/
            //Log.e("MyApp", "Turn on BT and pair with isi00");
            finish();
        }
        //registerReceiver(br, new IntentFilter(BluetoothDevice.ACTION_FOUND));

        for(BluetoothDevice bd : BluetoothAdapter.getDefaultAdapter().getBondedDevices()){
            //Log.e("MyApp",bd.getName());
            //Log.e("MyApp",bd.getName().substring(0,5));
            if( bd.getName().substring(0,5).equals(BT_TARGET_NAME)){
                device = bd;
                //Log.e("MyApp", "isi00 found");
                //Log.e("MyApp", device.getAddress());
                //UUID uuid = UUID.fromString(APP_UUID);
                uuid = UUID.fromString(myUUID);
                th = new MasterThread(uuid, device, MainActivity.this);
                //Log.e("MyApp", "th creato");
                th.start();
            } else {
                //Log.e("MyApp", "not found");
            }
        }


        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currFlux = 0;
                th.setFlux(currFlux);

            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           currFlux = 1;
                                           th.setFlux(currFlux);
                                       }
                                   }
        );
        button2.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           currFlux = 2;
                                           th.setFlux(currFlux);

                                       }
                                   }
        );
        button3.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           currFlux = 3;
                                           th.setFlux(currFlux);

                                       }
                                   }
        );


    }

    public void onDestroy(String msg){
        super.onDestroy();
        ConnectionManager.getInstance().cancel();
    }

    public void sendMessage(String msg){
        ConnectionManager.getInstance().write(msg.getBytes());
    }


    @Override
    public void onStart(){
        super.onStart();
        //btAdapter.startDiscovery();
    }

    @Override
    public void onStop(){
        super.onStop();
        //btAdapter.cancelDiscovery();
    }

    @Override
    public void onActivityResult(int reqID, int res, Intent data){
        /*if(reqID == ENABLE_BT_REQ && res == Activity.RESULT_OK){
            //BT Enabled
        }
        if(reqID == ENABLE_BT_REQ && res == Activity.RESULT_CANCELED){
            //BT enabling process aborted
        }*/
    }

    public EditText getEditText(){
        return this.umidita;
    }

    /*@Override
    public void onDestroy() {

        try{
            if(br!=null)
                unregisterReceiver(br);

        }catch(Exception e){}

        super.onDestroy();
    }*/

}
